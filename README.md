## README

This project was created following this [tutorial](https://css-tricks.com/a-dark-mode-toggle-with-react-and-themeprovider/)

<img style="width: 500px;" src="./public/preview.gif">

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
